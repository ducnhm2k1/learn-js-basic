var students = [
    {
        name: 'ÁNh Dương',  // Property: Value
        old: 18
    },
    {
        name: 'MyMy',  // Property: Value
        old: 12
    },
    {
        name: 'ÁNh Dương 2',  // Property: Value
        old: 186
    },
    {
        name: 'MyMy 2',  // Property: Value
        old: 126
    },
    {
        name: 'MyMy 3',  // Property: Value
        old: 126
    }
]

// For(), forEach()
if (students.length > 10) {
} else {
    for (var index = 0; index < students.length; index += 1) {
        // console.log('students: ', students[index])
        // console.log('index: ', index);
    }
}


var day;
switch (day) {
    case 0:
        day = "Sunday";
        break;
    case 1:
        day = "Monday";
        break;
    case 2:
        day = "Tuesday";
        break;
    case 3:
        day = "Wednesday";
        break;
    case 4:
        day = "Thursday";
        break;
    case 5:
        day = "Friday";
        break;
    case 6:
        day = "Saturday";
        break;
}

console.log(day)