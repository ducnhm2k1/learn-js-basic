//  Before 2015
//  BIến nguyên thủy
ducNhu = "bạn là nhất"; // type String
console.log(ducNhu);
number = 123;

boolean = true;

float = 30.5;

typeNull = null;

und = undefined;

string = "You are number one"

// Kiểu đối tượng
//  ---------------------
student = {
  name: 'DucNhu',  // Property: Value
  old: 12
} //  type object
//  ---------------------


//  ---------------------
arrays = [
  'a', 'b', 'c'
  //   0    1    2
] // n - 1
//  ---------------------


students = [
  {
    name: 'ÁNh Dương',  // Property: Value
    old: 12
  },
  {
    name: 'MyMy',  // Property: Value
    old: 12
  }
]




var data = [{
  // property: value
  "id": "1",
  "email": "ducnhu2k1@gmail.com",
  "address": "address 1c",
  "fullName": "fullName 1c",
  "skill": "Angular",
  "project": "<p>project 1ccd</p>\n",
  "birthday": "2021-03-14",
  "fileAvatar": "https://scontcdcdcent.fhph1-1.fna.fbcdn.net/v/t1.0-9/133549349_1281976232160042_6231890056107915313_n.jpg?_nc_cat=102&ccb=2&_nc_sid=09cbfe&_nc_ohc=UB0DLQI43pMAX_bPNdy&_nc_ht=scontent.fhph1-1.fna&oh=16b4625d90b7612a2b8d9426e0f96991&oe=60485215",
  "phone": 987654326545
}, {
  "id": "6",
  "email": "ducnhu2k1@gmail.com",
  "address": "Ha Noi",
  "fullName": "Nhữ Hoàng Minh Đức",
  "skill": "Time Management",
  "project": "<h2>Unrelated Components: Chia sẻ dữ liệu th&ocirc;ng qua Service</h2>\n\n<p>Đ&ocirc;i khi việc chia sẻ dữ liệu giữa c&aacute;c component kh&ocirc;ng li&ecirc;n quan tới nhau sẽ trở n&ecirc;n kh&oacute; khăn hơn. Như c&aacute;c component c&oacute; mối quan hệ sibling hay grandchildren, ... L&uacute;c n&agrave;y ch&uacute;ng ta sẽ phải d&ugrave;ng service để đồng bộ dữ liệu giữa c&aacute;c component. C&aacute;ch n&agrave;y đ&ograve;i hỏi bạn phải c&oacute; ch&uacute;t kiến thức về&nbsp;<strong>RxJS</strong>&nbsp;v&igrave; ch&uacute;ng ta sẽ cần d&ugrave;ng tới&nbsp;<strong>RxJS Subject</strong>&nbsp;hoặc&nbsp;<strong>RxJS BehaviorSubject</strong>. Tuy nhi&ecirc;n, ch&uacute;ng ta n&ecirc;n d&ugrave;ng&nbsp;<strong>RxJS BehaviorSubject</strong>&nbsp;thay v&igrave;&nbsp;<strong>RxJS Subject</strong>&nbsp;v&igrave; một số l&yacute; do:</p>\n\n<ul>\n\t<li>N&oacute; lu&ocirc;n lu&ocirc;n trả về một gi&aacute; trị</li>\n\t<li>N&oacute; c&oacute; phương thức&nbsp;<em><strong>getValue()</strong></em>&nbsp;gi&uacute;p ch&uacute;ng ta c&oacute; thể lấy ra được gi&aacute; trị mới nhất</li>\n\t<li>N&oacute; đảm bảo rằng component sẽ lu&ocirc;n nhận gi&aacute; trị mới nhất</li>\n</ul>\n\n<p>Trong&nbsp;<strong>DataService</strong>&nbsp;ch&uacute;ng ta khai b&aacute;o một thuộc t&iacute;nh&nbsp;<strong>BehaviorSubject</strong>&nbsp;để chứa gi&aacute; trị hiện tại của message. Tiếp đến, ch&uacute;ng ta định nghĩa một biến&nbsp;<strong>currentMessage</strong>&nbsp;để xử l&yacute; data stream như một&nbsp;<strong>observable</strong>. Biến n&agrave;y sẽ được sử dụng trong c&aacute;c component để lấy ra gi&aacute; trị hiện tại của message. Cuối c&ugrave;ng, ch&uacute;ng ta tạo một function để xử l&yacute; sự thay đổi của message.</p>\n\n<p>Sau đ&oacute;, c&aacute;c component sẽ inject&nbsp;<strong>DataService</strong>&nbsp;n&agrave;y v&agrave; tiến h&agrave;nh lấy ra gi&aacute; trị message đ&atilde; được đồng bộ, đồng thời ch&uacute;ng ta cũng c&oacute; thể thay đổi message bằng c&aacute;ch gọi tới function&nbsp;<strong>changeMessage()</strong>&nbsp;m&agrave; ta đ&atilde; định nghĩa trong service. V&agrave; tất nhi&ecirc;n, một khi ch&uacute;ng ta gọi tới function&nbsp;<strong>changeMessage()</strong>, th&igrave; message sẽ được cập nhật ở tất cả c&aacute;c component đ&atilde; được inject&nbsp;<strong>DataService</strong>.</p>\n",
  "birthday": "2021-03-17",
  "fileAvatar": "https://images.viblo.asia/avatar/e9dacc6b-0708-41d0-8735-d513681ebfb8.jpeg",
  "phone": 977010310
}]