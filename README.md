## <!--

75 Ho Tung Mau

- làm màu cho trang web
- FE:
  - làm animation
  - call api: AJAX => Axio
  - code logic
- BE:
  - Nodes
- Application:
  ReactNative
  ...
- Framework:
  Angular (Google)
  React (Fb)
  Vue (Tàu)

  ----------------- Lộ Trình -----------------

  Biến (variable)
  Kiểu dữ liệu (type)
  Toán Tử (tycal)
  Mảng (Array)

  Vòng lặp (Loop)
  Câu điều kiện (if, else)

  Switch (100%)
  Hàm(Function)

  Call Api (promise, Async/Await, Axio)
  Animation (DOM)

  => FrameWork
  => TTap

  -->
